===============================
pysol_cards
===============================

.. toctree::
   :maxdepth: 2

   README for pysol_cards <README>
   CONTRIBUTING
   LICENSE
   CHANGELOG

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
