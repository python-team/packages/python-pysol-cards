=====================
Appendix C. Changelog
=====================
:Info: This is the changelog for pysol_cards.
:Author: Shlomi Fish <shlomif@cpan.org>
:Copyright: © 2020, Shlomi Fish.
:License: BSD (see /LICENSE or :doc:`Appendix B <LICENSE>`.)
:Date: 2024-01-05
:Version: 0.16.0

.. index:: CHANGELOG

GitHub holds releases, too
==========================

More information can be found on GitHub in the `releases section
<https://github.com/shlomif/pysol_cards/releases>`_.

Version History
===============

0.1.0
    Initial release.
