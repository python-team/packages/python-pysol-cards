# -*- encoding: utf-8 -*-
# pysol_cards v0.16.0
# Deal PySol FC Cards
# Copyright © 2020, Shlomi Fish.
# See /LICENSE for licensing information.

"""
Main routine of pysol_cards.

:Copyright: © 2020, Shlomi Fish.
:License: BSD (see /LICENSE).
"""

__all__ = ('main',)


def main():
    """Main routine of pysol_cards."""
    print("Hello, world!")
    print("This is pysol_cards.")
    print("You should customize __main__.py to your liking (or delete it).")


if __name__ == '__main__':
    main()
