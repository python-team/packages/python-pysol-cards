Metadata-Version: 2.1
Name: pysol_cards
Version: 0.16.0
Summary: Deal PySol FC Cards
Home-page: https://github.com/shlomif/pysol_cards
Author: Shlomi Fish
Author-email: shlomif@cpan.org
License: 3-clause BSD
Keywords: pysol_cards
Platform: any
Classifier: Development Status :: 1 - Planning
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
License-File: LICENSE
License-File: AUTHORS

==============================================================================
pysol_cards.  Deal PySol FC Cards
==============================================================================
:Info: This is the README file for pysol_cards.
:Author: Shlomi Fish <shlomif@cpan.org>
:Copyright: © 2020, Shlomi Fish.
:Date: 2024-01-05
:Version: 0.16.0

.. index: README
.. image:: https://travis-ci.org/shlomif/pysol_cards.svg?branch=master
   :target: https://travis-ci.org/shlomif/pysol_cards

===============================
pysol_cards
===============================

Deal PySol FC Cards

The pysol-cards python modules allow the python developer to generate the
initial deals of some PySol FC games. It also supports PySol legacy deals
and Microsoft FreeCell / Freecell Pro deals.

* Free software: Expat license
* Documentation: pydoc
* Source: https://github.com/shlomif/pysol_cards
* Bugs: https://github.com/shlomif/pysol_cards/issues

--------

Example:
--------

::

        from pysol_cards.cards import CardRenderer
        from pysol_cards.deal_game import Game
        from pysol_cards.random_base import RandomBase
        ms24_str = Game(
            game_id="freecell",
            game_num=24,
            which_deals=RandomBase.DEALS_MS,
            max_rank=13
        ).calc_layout_string(
            CardRenderer(print_ts=True)
        )
        print(ms24_str, end='')

COPYRIGHT
---------
Copyright © 2020, Shlomi Fish.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions, and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions, and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

3. Neither the name of the author of this software nor the names of
   contributors to this software may be used to endorse or promote
   products derived from this software without specific prior written
   consent.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
